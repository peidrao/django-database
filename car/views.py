from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from datetime import datetime
from django.db import connection
from django.urls.base import reverse
from django.contrib import messages

from .models import Car


def create_car(request):
    if request.method == 'POST':
        plaque = request.POST.get('plaque')
        car_model = request.POST.get('car_model')
        color = request.POST.get('color')
        image_car = request.POST.get('image_car')
        price_day = request.POST.get('price_day').replace(',', '.')
        brand = request.POST.get('brand')
        image_car = request.POST.get('image_car')
        status_car = request.POST.get('status_car')
        initial_date = request.POST.get('initial_date')
        finish_date = request.POST.get('finish_date')
        created_at = datetime.now()
        updated_at = datetime.now()
        user_id = request.user.id

        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO car_car VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", [
                           None, image_car, car_model, brand, plaque, status_car, color, price_day, initial_date, finish_date,
                           created_at, updated_at, user_id, ])

        messages.success(request, 'Carro cadastrado com sucesso!')
        return HttpResponseRedirect(reverse('user:profile'))

    context = {
        'car_options': Car
    }

    return render(request, 'create_car.html', context)
