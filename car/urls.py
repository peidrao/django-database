from django.urls import path

from car import views

app_name = 'car'

urlpatterns = [
     path('create_car/', views.create_car, name='create_car'),
]
