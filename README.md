# Curso de Django com SQL

## 1 - Instalação do ambiente
-   ambiente virtual
-   .gitignore
-   requirements.txt
-   core
-   app home
    - Adicionar arquivo de urls
- primeira view index


## Adição de arquivos estáticos
- Template do projeto : https://www.free-css.com/free-css-templates/page260/e-store
- Organizar a estrutura do html

## Criação de usuário
- Criação de app user
- Criação de modelo, urls e view.
- criação de usuário

## Login
view de login

## logout & profile
- view de logout
- adicionar validação para botões de login, profile no header
- view profile, url, template

## app carro
- criar modelo, url, view, estruturar html

## view para criação de carro
- criar view
