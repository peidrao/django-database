from datetime import datetime
from django.db import connection

from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.urls import reverse

from user.models import User


def auth_login(request):
    if request.method == 'POST':
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)

        user = authenticate(email=email, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('user:profile'))
            else:
                return HttpResponse('Não foi possível realizar seu login')

    return render(request, 'login.html', {})


def create_user(request):
    if request.method == 'POST':
        username = request.POST.get("username")
        first_name = request.POST.get("first_name")
        email = request.POST.get("email")
        last_name = request.POST.get("last_name")
        phone = request.POST.get("phone")
        cpf = request.POST.get("cpf")
        address = request.POST.get('address')
        type_user = request.POST.get("type_user")
        password = request.POST.get("password1")
        password2 = request.POST.get("password2")
        last_login = datetime.now()
        is_superuser = 0
        is_active = 1
        is_staff = 0
        date_joined = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        if (password2 != password):
            messages.warning(request, 'Senhas não estão iguais')
            return HttpResponseRedirect(reverse('user:create_user'))

        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO user_user VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", [
                None, make_password(
                    password), last_login, is_superuser, is_staff, is_active, date_joined,
                username, first_name, last_name, cpf, address, phone, type_user, email])

        return HttpResponseRedirect(reverse('user:login'))


    context = {
       'user_list': User
    }

    return render(request, 'create_user.html', context)


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('home:index'))


def profile(request):
    return render(request, 'profile.html', {})
