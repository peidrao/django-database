from django.urls import path

from user import views

app_name = 'user'

urlpatterns = [
     path('login/', views.auth_login, name='login'),
     path('create_user/', views.create_user, name='create_user'),
     path('logout/', views.logout_user, name='logout'),
     path('profile/', views.profile, name='profile'),
]
